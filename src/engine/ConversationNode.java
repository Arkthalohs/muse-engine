package engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConversationNode {
	public Map<Response, ConversationNode> children;
	List<Response> responses;
	String speaker, line;
	List<String> scripts;
	
	public ConversationNode() {
		children = new HashMap<Response, ConversationNode>();
		responses = new ArrayList<Response>();
		scripts = new ArrayList<String>();
		
	}
	
	public ConversationNode(String str) {
		this();
		speaker = str;
	}
	
	public void addChild(Response r, ConversationNode child) {
		children.put(r, child);
		responses.add(r);
	}

	public String getSpeaker() {
		return Plot.parse(speaker, true);
	}
	
	public String getLine() {
		return Plot.parse(line, true);
	}
	
	public void entryExecute() {
		for(String s : scripts)
			Plot.execute(s);
	}
	
	@Override
	public String toString() {
		if(line == null)
			return speaker;
		return speaker + ":" + line + " " + children.keySet();
	}

	public List<Response> getResponses() {
		List<Response> list = new ArrayList<Response>();
		for(Response r : responses) {
			if(r.visible())
				list.add(r);
		}
		return list;
	}

	public ConversationNode say(Response string) {
		for(Response r : children.keySet())
			if(r.equals(string)) {
				ConversationNode cn = children.get(r);
				r.execute();
				cn.entryExecute();
				return cn;
			}
		return this;
	}
}

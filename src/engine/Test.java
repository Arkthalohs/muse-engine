package engine;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Test {

	
	public static void main(String[] args) {
		try {
			while(true) {
				Plot.reset();
				Scanner in = new Scanner(System.in);
				System.out.println("Please enter the name of file you wish to open (/exit to exit)");
				String filename = in.nextLine();
				if(filename.equals("/exit"))
					break;
				run(Conversation.loadConversation(filename));
				in.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void run(ConversationNode cn) {
		Scanner in = new Scanner(System.in);
		cn.entryExecute();
		ConversationNode cur = cn;
		while(cur != null) {
			System.out.println(cur.getSpeaker() + " : " + cur.getLine());
			List<Response> res = cur.getResponses();
			int i = 1;
			for(Response s : res)
				System.out.println("\t" + (i++) + ": " + s.getResponse());
			int r = in.nextInt() - 1;
			cur = cur.say(res.get(r));
		}
	}

}

package engine;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Plot {
	private static Map<String, String> variables;
	
	public static void reset() {
		variables = new HashMap<String, String>();
	}
	
	public static void put(String var, String val) {
		variables.put(var, val);
	}
	
	public static void execute(String command) {
		String[] parts = command.split("=");
		
		try {
			command = parse(parts[1].trim());
			variables.put(parts[0].trim(), "" + new ScriptEngineManager().getEngineByName("JavaScript").eval(command));
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
//		variables.put(parts[0].trim(), parts[1].trim());
	}
	
	public static boolean evaluate(String condition) {
		try {
			condition = parse(condition);
			return (Boolean)new ScriptEngineManager().getEngineByName("JavaScript").eval(condition);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return false;
		/** code for non script-engine based condition evaluation */
//		boolean flag = true;
//		for(String piece : condition.split("\\&\\&")) {
//			boolean pieceFlag = false;
//			for(String part : piece.split("\\|\\|"))
//				pieceFlag |= evaluatePiece(part);
//			flag &= pieceFlag;
//		}
//		return flag;
	}

	private static String parse(String s) {
		return parse(s, true);
	}
	
	public static String parse(String s, boolean replaceAll) {
		String[] words = s.split(" ");
		for(String word : words)
			if(variables.containsKey(word))
				s = s.replace(word, variables.get(word));
			else if(word.startsWith("$") && replaceAll)
				s = s.replace(word, "0");
		return s;
	}
	
	private static boolean evaluatePiece(String condition) {
		if(condition.contains("<=")) {
			String[] parts = condition.split("<=");
			return evaluateLessThan(parts[0].trim(), parts[1].trim()) ||
					evaluateEquality(parts[0].trim(), parts[1].trim());
		} else if(condition.contains(">=")) {
			String[] parts = condition.split(">=");
			return evaluateGreaterThan(parts[0].trim(), parts[1].trim()) ||
					evaluateEquality(parts[0].trim(), parts[1].trim());
		} else if(condition.contains("=")) {
			String[] parts = condition.split("==");
			return evaluateEquality(parts[0].trim(), parts[1].trim());
		} else if(condition.contains("<")) {
			String[] parts = condition.split("<");
			return evaluateLessThan(parts[0].trim(), parts[1].trim());
		} else if(condition.contains(">")) {
			String[] parts = condition.split(">");
			return evaluateGreaterThan(parts[0].trim(), parts[1].trim());
		}
		return false;
	}
	
	private static boolean evaluateEquality(String var, String expected) {
		String value = variables.get(var);
		if(value == null)
			value = "0";
		return value.equals(expected);
	}

	private static boolean evaluateLessThan(String var, String expected) {
		String value = variables.get(var);
		if(value == null)
			value = "0";
		return Double.parseDouble(value) < Double.parseDouble(expected);
	}
	
	private static boolean evaluateGreaterThan(String var, String expected) {
		String value = variables.get(var);
		if(value == null)
			value = "0";
		return Double.parseDouble(value) > Double.parseDouble(expected);
	}
}

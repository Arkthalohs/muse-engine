package engine;

import java.util.ArrayList;
import java.util.List;

public class Response {
	List<Conditional> conditions;
	List<Script> scripts;
	private String response;
	
	public Response() {}
	
	public Response(String r) {
		response = r;
		scripts = new ArrayList<Script>();
		conditions = new ArrayList<Conditional>();
	}
	
	public String getResponse() {
		return Plot.parse(response, true);
	}
	
	public void addScript(String line) {
		if(line.startsWith("if"))
			conditions.add(new Conditional(line.substring(2)));
		if(line.startsWith("set"))
			scripts.add(new Script(line.substring(3)));
	}
	
	public boolean visible() {
		boolean vis = true;
		for(Conditional c : conditions)
			vis &= c.eval();
		return vis;
	}
	
	public void execute() {
		for(Script script : scripts)
			script.execute();
	}
	
	@Override
	public String toString() {
		String out = "";
		out += "if(" + conditions + ") ";
		out += response;
		out += " {" + scripts + "}";
		return out;
	}
	
	private class Conditional {
		String condition;
		
		public Conditional(String c) {
			condition = c.trim();
		}
		
		public boolean eval() {
			return Plot.evaluate(condition);
		}
	}
	
	private class Script {
		String command;
		public Script(String c) {
			command = c.trim();
		}
		
		public void execute() {
			Plot.execute(command);
		}
	}
}
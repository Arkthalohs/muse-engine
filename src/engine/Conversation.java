package engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Conversation {
	private static String EXT = ".conv";
	private static Map<String, ConversationNode> conversations;
	
	public Conversation() {
	}

	/**loads a conversation from the specified file
	 * 
	 * @param filename name of file to use
	 * @return conversation generated from file
	 * @throws FileNotFoundException if filename isn't found
	 */
	public static ConversationNode loadConversation(String filename) throws FileNotFoundException {
		if(conversations == null)
			conversations = new HashMap<String, ConversationNode>();
		File f = new File(filename);
		Scanner in = new Scanner(f);
		Map<String, ConversationNode> nodes = new HashMap<String, ConversationNode>();
		
		while(in.hasNext()) {
			String line = in.nextLine();
			line = line.replace("Node ", "").replace("{", "").trim();
			ConversationNode n = parseNode(in);
			nodes.put(line, n);
		}
		in.close();
		
		conversations.put(filename, nodes.get("START"));
		
		for(String key : nodes.keySet()) {
			ConversationNode cn = nodes.get(key);
			for(Response r : cn.children.keySet()) {
				if(cn.children.get(r).line == null) {
					String k = cn.children.get(r).speaker;
					if(k.endsWith(EXT)) {
						if(conversations.containsKey(k))
							cn.children.put(r, conversations.get(k));
						else
							cn.children.put(r, loadConversation(k));
					} else
						cn.children.put(r, nodes.get(k));
				}
			}
		}
		
		ConversationNode cn = nodes.get("START");
		return cn;
	}
	
	private static ConversationNode parseNode(Scanner in) {
		String line = in.nextLine().trim();
		ConversationNode cn = new ConversationNode();
		
		while(!line.equals("}")) {		
			if(line.startsWith("Speaker:"))
				cn.speaker = line.substring(8);
			if(line.startsWith("Them:"))
				cn.line = line.substring(5);
			if(line.startsWith("OnEntry:")) {
				line = in.nextLine();
				while(line.startsWith("\t\t")) {
					cn.scripts.add(line.trim().substring(4));
					
					line = in.nextLine();
				}
				line = line.trim();
				continue;
			}
			if(line.startsWith("Responses:")) {
				line = in.nextLine();
				while(line.startsWith("\t\t")) {
					String[] parts = line.trim().split("->");
					Response r = new Response(parts[0]);
					
					line = in.nextLine();
					while(line.startsWith("\t\t\t")) {
						r.addScript(line.trim());
						line = in.nextLine();
					}
					String targ = "";
					if(parts.length > 1) targ = parts[1];
					
					cn.addChild(r, new ConversationNode(targ));
				}
				line = line.trim();
				continue;
			}
			
			line = in.nextLine().trim();
		}

		return cn;
	}
}

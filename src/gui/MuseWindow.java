package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import engine.Conversation;
import engine.ConversationNode;
import engine.Response;

public class MuseWindow extends JPanel {
	private static final long serialVersionUID = -3346418994358500673L;
	List<MuseNode> nodes;

	public MuseWindow() {
		setPreferredSize(new Dimension(640, 480));
		nodes = new ArrayList<MuseNode>();
		try {
			ConversationNode cn = Conversation.loadConversation("test.conv");
			traverse(cn, nodes, 320, 20);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	private void traverse(ConversationNode cn, List<MuseNode> list, int x, int y) {
		if(cn != null && !list.contains(new MuseNode(cn, 0, 0))) {
			list.add(new MuseNode(cn, x, y));
			
			x = 320 - 60 * cn.children.keySet().size();
			int i = 0;
			for(Response c : cn.children.keySet()) {
				traverse(cn.children.get(c), list, x + 60 * i, y + 24 + 24 * i);
				i++;
			}
		}
	}
	
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		for(MuseNode mn : nodes)
			mn.draw(g2);
	}
	
	private class MuseNode {
		ConversationNode cn;
		int x, y;
		public MuseNode(ConversationNode c, int x, int y) {
			cn = c;
			this.x = x;
			this.y = y;
		}
		
		public void draw(Graphics2D g) {
			g.drawString(cn.toString(), x, y);
			
			for(MuseNode mn : nodes) {
				if(cn.children.values().contains(mn.cn))
					g.drawLine(x, y, mn.x, mn.y);
			}
		}
		
		public boolean equals(Object e) {
			if(e instanceof ConversationNode)
				return e == cn;
			if(e instanceof MuseNode)
				return ((MuseNode) e).cn == cn;
			return false;
		}
	}

}

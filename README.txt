How To Use Muse

I. Writing a conversation
There is not currently any UI to create a conversation, so you must write
one yourself. Muse Conversation files should be of the form filename.conv
This is so you can properly jump between different files.

a. Node Declaration
A conv file is made up of many different nodes, each of which has a unique
label. The form for a node is as follows:
Node label {
	content (all content is indented at least once)
}
You can have as many nodes as you want, but you must have one node with the
label "START", this is the first node the conversation will use.

b. Being Told Things
The line in a node delivered by an NPC is determined in two ways. First, 
there is the question of who is speaking. This is determined with the Speaker 
tag. For example, this line
	Speaker:President Barack Obama
would mean that the name/title of the NPC is "President Barack Obama".
The second part of the line is the actual piece of dialogue. This is determined
with the Them tag. That is,
	Them:Hello, my name is Barack Obama.
will mean that the dialogue in this node is "Hello, my name is Barack Obama"

c. Responding
To begin listing possible player responses, the tag Responses is used, then
responses themselves are indented an additional time (and conditionals and
scripts an additional time beyond that). 
To have a response go to a node, the "->" string is used in the form
		response->node_label
where the player's response "response" will enter the node with the label of
"node_label".
For example,
	Responses:
		Yeah, right.->doesntBelieve
		Oh my god, the president!->believes
means that the player can say either "Yeah, right." or "Oh my god, the president!" 
which go to the nodes labeled doesntBelieve and believes, respectively.

c. Entry Scripting
The OnEntry tag can be used to set variables upon entry to a node. Currently, all
script lines must begin with the word "set", followed by a variable name, an equals
sign, and an expression to store in that variable.  There must be spaces around 
variable names.All lines which are indented twice below it will be treated as 
scripts and executed when the node is entered.
	OnEntry:
		set $var = "Fishcakes"
will mean that the variable $var will have the value "Fishcakes".

d. Response Scripting
Response scripting works much the same as entry scripting, except that the script
will be executed when the target response is used. Scripts to be executed upon
a specific response should be below the response and indented 3 times.

e. Response Conditional
A response can be made visible under certain circumstances with a conditional.
Conditionals are laid out the same way as response scripts, but begin with the
keyword "if". Like scripts, conditionals require spaces around variable names.

f. Jumping to another conversation
You can enter an entirely different conversation through a response by giving
that conversation's filename, rather than the label of a node. For example
		Shut up.->assaultBySecretService.conv
would mean that by saying "Shut up.", the conversation would seamlessly transition
to the assaultBySecretService.conv conversation, starting at the START node.

g. Ending a conversation
To end a conversation, use the label END in a response. This will signal that
there is no node to proceed to, and will instead end the conversation.
For example
		I should run away from the men with guns->END
will end the conversation.

h. An Example Conversation

Node START {
	OnEntry:
		set $loop = 1
	Speaker:Steve McBadass
	Them:Hello
	Responses:
		Who are you?->node2 
		Hi?->node2 
}
Node node2 {
	Speaker:Steve McBadass
	Them:I'm Commander McBadass. You've looped $loop times.
	Responses:
		Once again?->START
			set $loop = $loop + 1
		Continue->conversationEnd 
			if $loop == 3
		Go elsewhere->another.conv
			if $loop >= 0 && $loop < 3
}
Node conversationEnd {
	Speaker:Steve McBadass
	Them:Let's continue, then.
	Responses:
		I should go.->END
}


II. Running a conversation
To run a conversation, the only currently supported option is through
command-line interaction. To do this, run the main method in Test.java,
and enter the name of the conversation you wish to explore. Traversal is
performed through entering the numbers on the left of each possible 
response.


III. TODO
-interrupts (ala Mass Effect's paragon/renegade interrupts in ME2 and 3)
-party members
-party member influence based on response attributes 
	(i.e. Samuel likes when you say Kind things)
-made scripts and conditionals support cases where the variable name doesn't have
	spaces around it
